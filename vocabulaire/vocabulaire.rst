.. index::
   pair: Vocabulaire; Persan
   ! Vocabulaire

.. _vocabulaire:

=====================
Vocabulaire
=====================

.. glossary::


   Femme
       Zan, زن

   Vie
      Zendegi, زندگی

   Liberté
       Âzâdi, آزادی

