.. index::
   pair: Description ; Norouz

.. _norouz_desc:

=====================
Description
=====================


Définition wikipedia
====================

- https://fr.wikipedia.org/wiki/Norouz

Norouz (en persan: نوروز nowruz) est la fête traditionnelle des peuples iraniens
qui célèbrent le nouvel an du calendrier persan (premier jour du printemps).

La fête est célébrée par certaines communautés le 21 mars et par d'autres
le jour de l'équinoxe vernal, dont la date varie entre le 20 et le 22 mars.

Norouz a des origines iraniennes et zoroastriennes ; cependant, depuis plus
de 3000 ans, cette fête est célébrée par diverses communautés en Asie de l'Ouest,
Asie centrale, Caucase, bassin de la mer Noire, Balkans et Asie du sud.

C'est une fête culturelle et religieuse (voir Zoroastrianisme et Baha'i)

En français, Norouz est également appelé Nouvel An iranien ou Nouvel An persan5,6.

Le Norouz est inscrit à l'inventaire du patrimoine culturel immatériel en France
en 2019


Définition kurde
====================

- https://anfenglishmobile.com/news/the-story-of-kawa-and-newroz-25623

Although celebrated by others, Newroz is especially important for the Kurds
as it is also the start of the Kurdish calendar and celebrates the Kurds own
long struggle for freedom.



